% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/confmat.R
\name{NPVfromConfusion}
\alias{NPVfromConfusion}
\title{Calculate NPV Given the Confusion Matrix}
\usage{
NPVfromConfusion(x, case = 2, predictionsin = "rows")
}
\arguments{
\item{x}{2x2 matrix or data.frame.  Typically the output of \code{link[base]{table}}}

\item{case}{class label of the cases.}

\item{predictionsin}{character in \code{c("rows", "cols")}.}
}
\value{
numerical.  the NPV
}
\description{
Calculate NPV Given the Confusion Matrix
}
\examples{
## draw data
n <- 50
truth      <- sample(c("control", "case"), size = n, replace = TRUE)
prediction <- sample(c("control", "case"), size = n, replace = TRUE)

## confusion matrix
table(truth, prediction)

## predictions in columns
NPVfromConfusion(table(truth, prediction), case = "case", predictionsin = "cols")

## predictions in rows
NPVfromConfusion(table(prediction, truth), case = "case", predictionsin = "cols")
}
\author{
Dr. Andreas Leha
}
