% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/roc.R
\name{youdenConfmat}
\alias{youdenConfmat}
\title{extract a confusion matrix from roc object}
\usage{
youdenConfmat(rocobj)
}
\arguments{
\item{rocobj}{roc objcet from \code{\link[pROC]{roc}}}
}
\value{
data.frame.  results of \code{\link{confmat2df}}
}
\description{
extract a confusion matrix from roc object
}
\examples{
## draw data
n <- 50
truth      <- sample(c("control", "case"), size = n, replace = TRUE)
prediction <- runif(n = n)

## generate roc object
rocobj <- pROC::roc(truth, prediction)

## confusion matrix at Youden index
youdenConfmat(rocobj)
}
\author{
Dr. Andreas Leha
}
