

<!-- Readme.md is generated from Readme.org. Please edit that file -->


# binclaperf R package

This package provides some functionality for easy assessing of
(binary) classification performance.

## Installation

Installation directly from gitlab is possible, when the package
[devtools](https://cran.r-project.org/package=devtools) is installed.

Once the dependencies are installed, get this package with

```R
library("devtools")
install_git("https://gitlab.gwdg.de/aleha/binclaperf")
```

## Roadmap


<!--
Local Variables:
 mode: gfm
 markdown-command: "marked"
End:
-->
