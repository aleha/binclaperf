##' Calculate Accurcy Given the Confusion Matrix
##'
##' @param x 2x2 matrix or data.frame.  Typically the output of \code{link[base]{table}}
##' @return numerical.  the accuracy
##' @author Dr. Andreas Leha
##' @export
##' @examples
##' ## draw data
##' n <- 50
##' truth      <- sample(c("control", "case"), size = n, replace = TRUE)
##' prediction <- sample(c("control", "case"), size = n, replace = TRUE)
##'
##' ## confusion matrix
##' table(truth, prediction)
##'
##' ACCfromConfusion(table(truth, prediction))
ACCfromConfusion <- function(x)
{
  sum(diag(x)) / sum(x)
}

##' Calculate Prevalence Given the Confusion Matrix
##'
##' @param x 2x2 matrix or data.frame.  Typically the output of \code{link[base]{table}}
##' @param case class label of the cases.
##' @param predictionsin character in \code{c("rows", "cols")}.
##' @return numerical.  the prevalence
##' @author Dr. Andreas Leha
##' @export
##' @examples
##' ## draw data
##' n <- 50
##' truth      <- sample(c("control", "case"), size = n, replace = TRUE)
##' prediction <- sample(c("control", "case"), size = n, replace = TRUE)
##'
##' ## confusion matrix
##' table(truth, prediction)
##'
##' PREVfromConfusion(table(truth, prediction))
PREVfromConfusion <- function(x, case = 2, predictionsin = "rows")
{
  if (predictionsin == "cols") {
    PREVfromConfusion(t(x), case = case, predictionsin = "rows")
  } else {
    sum(x[,case]/sum(x))
  }
}


##' Calculate Sensitivity Given the Confusion Matrix
##'
##' @param x 2x2 matrix or data.frame.  Typically the output of \code{link[base]{table}}
##' @param case class label of the cases.
##' @param predictionsin character in \code{c("rows", "cols")}.
##' @return numerical.  the sensitivity
##' @author Dr. Andreas Leha
##' @export
##' @examples
##' ## draw data
##' n <- 50
##' truth      <- sample(c("control", "case"), size = n, replace = TRUE)
##' prediction <- sample(c("control", "case"), size = n, replace = TRUE)
##'
##' ## confusion matrix
##' table(truth, prediction)
##'
##' ## predictions in columns
##' SENSfromConfusion(table(truth, prediction), case = "case", predictionsin = "cols")
##'
##' ## predictions in rows
##' SENSfromConfusion(table(prediction, truth), case = "case", predictionsin = "cols")
SENSfromConfusion <- function(x, case = 2, predictionsin = "rows")
{
  if (predictionsin == "cols") {
    SENSfromConfusion(t(x), case = case, predictionsin = "rows")
  } else {
    x[case,case] / sum(x[,case])
  }
}



##' Calculate Specificity Given the Confusion Matrix
##'
##' @param x 2x2 matrix or data.frame.  Typically the output of \code{link[base]{table}}
##' @param case class label of the cases.
##' @param predictionsin character in \code{c("rows", "cols")}.
##' @return numerical.  the specificity
##' @author Dr. Andreas Leha
##' @export
##' @examples
##' ## draw data
##' n <- 50
##' truth      <- sample(c("control", "case"), size = n, replace = TRUE)
##' prediction <- sample(c("control", "case"), size = n, replace = TRUE)
##'
##' ## confusion matrix
##' table(truth, prediction)
##'
##' ## predictions in columns
##' SPECfromConfusion(table(truth, prediction), case = "case", predictionsin = "cols")
##'
##' ## predictions in rows
##' SPECfromConfusion(table(prediction, truth), case = "case", predictionsin = "cols")
SPECfromConfusion <- function(x, case = 2, predictionsin = "rows")
{
  if (predictionsin == "cols") {
    SPECfromConfusion(t(x), case = case, predictionsin = "rows")
  } else {
    noncase <- {
      if (is.numeric(case)) {
        ifelse(case == 2, 1, 2)
      } else {
        setdiff(rownames(x), case)
      }
    }
    x[noncase,noncase] / sum(x[,noncase])
  }
}


##' Calculate Positive Predictive Value (PPV) Given the Confusion Matrix
##'
##' @param x 2x2 matrix or data.frame.  Typically the output of \code{link[base]{table}}
##' @param case class label of the cases.
##' @param predictionsin character in \code{c("rows", "cols")}.
##' @return numerical.  the PPV
##' @author Dr. Andreas Leha
##' @export
##' @examples
##' ## draw data
##' n <- 50
##' truth      <- sample(c("control", "case"), size = n, replace = TRUE)
##' prediction <- sample(c("control", "case"), size = n, replace = TRUE)
##'
##' ## confusion matrix
##' table(truth, prediction)
##'
##' ## predictions in columns
##' PPVfromConfusion(table(truth, prediction), case = "case", predictionsin = "cols")
##'
##' ## predictions in rows
##' PPVfromConfusion(table(prediction, truth), case = "case", predictionsin = "cols")
PPVfromConfusion <- function(x, case = 2, predictionsin = "rows")
{
  if (predictionsin == "cols") {
    PPVfromConfusion(t(x), case = case, predictionsin = "rows")
  } else {
    ## prevalence
    preva <- sum(x[,case]) / sum(x)

    PPV <- (SENSfromConfusion(x, case = case, predictionsin = predictionsin) * preva) /
      (SENSfromConfusion(x, case = case, predictionsin = predictionsin) * preva +
       (1 - SPECfromConfusion(x, case = case, predictionsin = predictionsin)) * (1 - preva))

    PPV
  }
}


##' Calculate NPV Given the Confusion Matrix
##'
##' @param x 2x2 matrix or data.frame.  Typically the output of \code{link[base]{table}}
##' @param case class label of the cases.
##' @param predictionsin character in \code{c("rows", "cols")}.
##' @return numerical.  the NPV
##' @author Dr. Andreas Leha
##' @export
##' @examples
##' ## draw data
##' n <- 50
##' truth      <- sample(c("control", "case"), size = n, replace = TRUE)
##' prediction <- sample(c("control", "case"), size = n, replace = TRUE)
##'
##' ## confusion matrix
##' table(truth, prediction)
##'
##' ## predictions in columns
##' NPVfromConfusion(table(truth, prediction), case = "case", predictionsin = "cols")
##'
##' ## predictions in rows
##' NPVfromConfusion(table(prediction, truth), case = "case", predictionsin = "cols")
NPVfromConfusion <- function(x, case = 2, predictionsin = "rows")
{
  if (predictionsin == "cols") {
    NPVfromConfusion(t(x), case = case, predictionsin = "rows")
  } else {
    ## prevalence
    preva <- sum(x[,case]) / sum(x)

    NPV <- (SPECfromConfusion(x, case = case, predictionsin = predictionsin) * (1 - preva)) /
      (SPECfromConfusion(x, case = case, predictionsin = predictionsin) * (1 - preva) +
       (1 - SENSfromConfusion(x, case = case, predictionsin = predictionsin)) * preva)

    NPV
  }
}


##' Calculate Performance Given the Confusion Matrix
##'
##' @param x 2x2 matrix or data.frame.  Typically the output of \code{link[base]{table}}
##' @param case class label of the cases.
##' @param predictionsin character in \code{c("rows", "cols")}.
##' @return data.frame.  the accuracy, sensitivity, specificity, PPV, NPV
##' @author Dr. Andreas Leha
##' @export
##' @examples
##' ## draw data
##' n <- 50
##' truth      <- sample(c("control", "case"), size = n, replace = TRUE)
##' prediction <- sample(c("control", "case"), size = n, replace = TRUE)
##'
##' ## confusion matrix
##' table(truth, prediction)
##'
##' ## predictions in columns
##' PERFfromConfusion(table(truth, prediction), case = "case", predictionsin = "cols")
##'
##' ## predictions in rows
##' PERFfromConfusion(table(prediction, truth), case = "case", predictionsin = "rows")
PERFfromConfusion <- function(x, case = 2, predictionsin = "rows")
{
  if (predictionsin == "cols") {
    PERFfromConfusion(t(x), case = case, predictionsin = "rows")
  } else {
    data.frame(ACC  =  ACCfromConfusion(x),
               SENS = SENSfromConfusion(x, case = case, predictionsin = predictionsin),
               SPEC = SPECfromConfusion(x, case = case, predictionsin = predictionsin),
               PPV  =  PPVfromConfusion(x, case = case, predictionsin = predictionsin),
               NPV  =  NPVfromConfusion(x, case = case, predictionsin = predictionsin),
               PREV = PREVfromConfusion(x, case = case, predictionsin = predictionsin))
  }
}


##' Calculate AUC Given the Confusion Matrix
##'
##' @param x 2x2 matrix or data.frame.  Typically the output of \code{link[base]{table}}
##' @param predictionsin character in \code{c("rows", "cols")}.
##' @return data.frame as returned by \code{\link{pROCaucdf}}
##' @author Dr. Andreas Leha
##' @export
##' @examples
##' ## draw data
##' n <- 50
##' truth      <- sample(c("control", "case"), size = n, replace = TRUE)
##' prediction <- sample(c("control", "case"), size = n, replace = TRUE)
##'
##' ## confusion matrix
##' table(truth, prediction)
##'
##' ## predictions in columns
##' AUCfromConfusion(table(truth, prediction), predictionsin = "cols")
AUCfromConfusion <- function(x, predictionsin = "rows")
{
  if (predictionsin == "cols") {
    AUCfromConfusion(t(x), predictionsin = "rows")
  } else {
    ##          prediction
    ## truth     case control
    ##   case       a       b
    ##   control    c       d

    aaa <- tribble(~truth, ~prediction,
                   1, 1,
                   2, 1,
                   1, 2,
                   2, 2)

    idx <- c(rep(1, x[1,1]), rep(2, x[1,2]),
             rep(3, x[2,1]), rep(4, x[2,2]))

    aaa <- aaa[idx,]

    rocobj <- purrr::possibly(pROC::roc, otherwise = NULL)(aaa$truth, aaa$prediction, ci = TRUE)

    pROCaucdf(rocobj)
  }
}



##' Calculate Performance including AUC Given the Confusion Matrix
##'
##' @param x 2x2 matrix or data.frame.  Typically the output of \code{link[base]{table}}
##' @param case class label of the cases.
##' @param predictionsin character in \code{c("rows", "cols")}.
##' @return data.frame.  the accuracy, sensitivity, specificity, PPV, NPV
##' @author Dr. Andreas Leha
##' @export
##' @examples
##' ## draw data
##' n <- 50
##' truth      <- sample(c("control", "case"), size = n, replace = TRUE)
##' prediction <- sample(c("control", "case"), size = n, replace = TRUE)
##'
##' ## confusion matrix
##' table(truth, prediction)
##'
##' ## predictions in columns
##' PERFandAUCfromConfusion(table(truth, prediction), case = "case", predictionsin = "cols")
##'
##' ## predictions in rows
##' PERFandAUCfromConfusion(table(prediction, truth), case = "case", predictionsin = "rows")
PERFandAUCfromConfusion <- function(x, case = 2, predictionsin = "rows")
{
  PERFfromConfusion(x, case = case, predictionsin = predictionsin) %>%
    bind_cols(AUCfromConfusion(x, predictionsin = predictionsin))
}


##' Format Matrix as data.frame
##'
##' @param x 2x2 matrix.
##' @return data.frame with proper column and row names
##' @author Dr. Andreas Leha
##' @export
##' @examples
##' ## draw data
##' n <- 50
##' truth      <- sample(c("control", "case"), size = n, replace = TRUE)
##' prediction <- sample(c("control", "case"), size = n, replace = TRUE)
##'
##' ## confusion matrix
##' table(truth, prediction)
##'
##' confmat2df(table(truth, prediction))
confmat2df <- function(x)
{
  res <-
    x %>%
    as.data.frame.matrix

  dnames <- names(dimnames(x))
  seps <- sapply(dimnames(x), function(ddd) any(grepl("^[<>=]", ddd)))
  seps <- c(" = ", " ")[as.numeric(seps) + 1]

  colnames(res) <- paste(dnames[2], colnames(res), sep = seps[2])

  res <- res %>%
    tibble::rownames_to_column(" ")

  res <- res %>%
    dplyr::mutate(` ` = paste(dnames[1],  .$` `, sep = seps[1]))

  res
}


##' Take a confmat (as data.frame) and add row/col sums
##'
##' Append a column with rowsums and a row with colsums
##' @param x 2x2 data.frame in the format as produced by \code{\link{confmat2df}}
##' @return data.frame 3x3 with added row/column
##' @author Dr. Andreas Leha
##' @export
##' @examples
##' ## draw data
##' n <- 50
##' truth      <- sample(c("control", "case"), size = n, replace = TRUE)
##' prediction <- sample(c("control", "case"), size = n, replace = TRUE)
##'
##' ## confusion matrix
##' confmat <-table(truth, prediction)
##'
##' ## confusion matrix as data.frame
##' confdf <- confmat2df(confmat)
##'
##' ## with sums
##' confmatdfAddSums(confdf)
confmatdfAddSums <- function(x)
{
  ## add sum column
  x <- x %>% mutate(.SUM = apply(x[,-1], 1, sum))
  colnames(x) <- colnames(x) %>% gsub("^.SUM$", "\u03a3", .)
  ## generated sum row
  bottomrow <- x[,-1] %>% summarise_all(sum) %>% mutate(` ` = "\u03a3")
  ## add sum row
  x <- x %>% bind_rows(bottomrow)
  ## return
  x
}
